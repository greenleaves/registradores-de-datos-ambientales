# EcoSen - Registradores de datos abiertos (o lo más abiertos posibles) para variables ambientales (English below)
El monitoreo ambiental es esencial para el cuidado del ambiente. Particularmente, me interesa poder monitorear ambientes acuáticos. Para medir estas variables una opción son los registradoresd e datos privativos. Estos debido a sus altos costos, o por ser de diseños cerrados nos imposibilitan compararlo o repararlos.
Nuestra propuesta consiste en el diseño y desarrollo de registradores de datos (RD) con tecnologías inalámbricas para tener información en tiempo real en cualquier dispositivo conectado a internet. Los RD pueden ser tanto acuáticos como terrestres. Los acuáticos se espera puedan medir información relevante para el cálculo de índices de calidad de agua ([Pesce y Wunderlin, 2000](https://www.sciencedirect.com/science/article/pii/S0043135400000361)), como ser turbidez, oxígeno disuelto, conductividad y pH. El terrestre va a medir aquellas variables relevantes para la calidad del aire o suelo. 
La base técnica para la construcción de estos registradores es la presente en el increíble proyecto [Cave Pearl Proyect](https://thecavepearlproject.org/), pero adaptandolo a aguas superficiales.

### Descargo de responsabilidad
Este proyecto se encuentra en gestación por lo que vamos a ver es su evolución. Aún no hemos logrado un perfecto funcionamiento de lo propuesto ni con todo tipo de sensores ni con la conectividad inalámbrica, así que cualquier persona que se quiera sumar es bienvenida. Puedes comuncarte con alejo.bonifacio@unc.edu.ar

## Motivación
Queremos traer a la sociedad la pasión por el monitoreo de ríos, lagunas, lagos y todo tipo de humedales. Además, reconocemos la importacia de aprender acerca de proyectos de electronica abierta con la posibilidad de reproducir, modificar y personalizar los RD. La tecnología abierta que queremos compartir es asociada a repositorios que harán de la información accesible a cualquier persona. Si en algún momento crees que alguna información no es lo suficiente pública por favor nos lo haces saber.

## Primer paso, midamos algo..
Vamos a usar una placa arduino para registrar unos valores del ambiente. Con esto vamos a lograr ver que estos microcontroladores son capaces de sensar variables ambientales y eso no interesa mucho.

### Materiales (con links a proveedores en Argentina)
* [Arduino uno](https://articulo.mercadolibre.com.ar/MLA-719198757-arduino-uno-r3-con-cable-usb-y-chip-desmontable-atmel-_JM#position=9&search_layout=grid&type=item&tracking_id=e573198c-48a2-4803-9afd-36106603708a)
* [DHT 11](https://articulo.mercadolibre.com.ar/MLA-752254138-modulo-sensor-humedad-relativa-y-temperatura-dht11-arduino-_JM#position=2&search_layout=stack&type=item&tracking_id=45f07ec5-ad22-412b-94d2-94497e139acf)
* [Cables dupont](https://articulo.mercadolibre.com.ar/MLA-621254141-pack-40-cables-macho-macho-10cm-dupont-arduino-y-protoboard-_JM#position=3&search_layout=stack&type=item&tracking_id=f217ef53-0080-491f-b6bd-d42105134adf)
* [Protoboard](https://articulo.mercadolibre.com.ar/MLA-630797518-protoboard-830-puntos-placa-experimental-arduino-electronica-_JM#position=1&search_layout=grid&type=item&tracking_id=1832eed4-aa24-40df-b3ca-a32b95b5c628)
* Cable USB-b a USB-c (viene incluido con el arduino UNO)
* Computadora de escritorio o laptop


### Procedimiento :electric_plug:
* Instalamos Arduino IDE en nuestra pc -> [link](https://www.arduino.cc/en/software). Abrimos el software. Para usar el programa en español, vamos a Archivo/Preferencias y seleccionamos Español en el idioma del editor.
* Conectamos la placa a la PC con el cable USB-b a USB-c.
* Abrimos el programa Arduino IDE, vamos al menú Herramientas/Puerto, nos fijamos qué puerto se nos habilita y lo seleccionamos. 
* Vamos al menú Herramientas/Placa y nos aseguramos que esté seleccionada la placa Arduino Uno.
* Abrimos el ejemplo blink, que se ubica en Archivo/Ejemplo/01.Basics. Subimos el código a la placa con la flecha que se ubica a la derecha en la parte superior de la consola. Si queremos aprender del código, podemos cambiar el tiempo en el que se mantiene prendido y/o apagado el LED cambiando el número que hay dentro de cada uno de los delay que hay en el código. Vamos al IDE y cambiamos el primero a 500 y el segundo a 2000. Volvemos a subir el código y chequeamos que la luz se mantenga apagada menos tiempo que prendida.
* Desconectamos la placa de la PC y armamos el circuito con la protoboard, el Arduino Uno y el DHT11. 
El esquema de conexión va a ser el siguiente:
* + (calle roja) -> 5V (VCC) en Arduino Uno
* - (calle azul) -> GND en Arduino Uno
* OUT del DHT11 -> pin 8 en Arduino Uno

* Volvemos a conectar la placa a la PC y revisamos en el IDE que el puerto y el tipo de placa sean los correctos
* Esto no es un paso sino una advertencia. Lo que sigue es empezar a probar códigos que usan librerías. Acá la cosa puede empezar a ponerse algo frustrante. Esto es porque puede haber muchas librerías para un mismo módulo pero no todas funcionan de la misma manera. Entonces, vamos a tener que estar muy segures de que la librerías sea la que funciona con nuestro código. Para evitar problemas, acá están los enlaces para descargar las librerías necesarias para los códigos que vamos a usar.
* Una vez armado el circuito, vamos a probar el siguiente [código](https://drive.google.com/drive/folders/1apZFteFHHeEfqCXaeWlCMYO2od5rfOG3?usp=sharing). Para esto:
* Bajamos el archivo del código y lo abrimos en el IDE. 
* Agregamos las librerías DHT sensor library y Adafruit sensor master. Para incluir estas librerías tenemos que descargar los archivos ZIP de cada una de estas ([DHT](https://github.com/adafruit/DHT-sensor-library) y [Adafruit](https://github.com/adafruit/Adafruit_Sensor)). En el IDE, vamos a Programa/Incluir librería/Añadir biblioteca .ZIP. Ahí cargamos las librerías recientemente descargadas sin descomprimir. Luego de esto vamos a Programa/Incluir librería/ y buscamos DHT y Adafruit, ambas al fondo de la lista desplegable. 
* Cargamos el código en la placa y abrimos el monitor serial con la lupita que se ubica a la derecha en la parte superior de la consola y comenzamos a ver los valores de humedad y temperatura. Nos aseguramos que la frecuencia que está en la parte inferior derecha del monitor serial esté en 9600.

## Segundo paso, Registremos... :arrow_up:
Para empezar a registrar vamos a necesitar un nuevo elemento, el módulo SD. Con este módulo empezaremos a ganar independencia, ya que podremos guardar la información generada por el sensor en una memoria extraíble. Así, podremos retirar la información de la placa para leerla en otro dispositivo.

### Materiales, muchos ya los tenemos :sunglasses:
* Circuito con la protoboard, el Arduino Uno y el DHT11 que armamos en el primer paso
* [Módulo micro SD](https://articulo.mercadolibre.com.ar/MLA-905429694-modulo-lector-memorias-micro-sd-arduino-5v-hobbytronica-_JM#position=3&search_layout=grid&type=item&tracking_id=78f54e44-642d-4fc8-b9fd-e26b2e29e181)
* [Memoria micro SD](https://articulo.mercadolibre.com.ar/MLA-927190694-memoria-micro-sd-2gb-nuevas-sin-blister-originales-100-_JM#position=4&search_layout=stack&type=item&tracking_id=d6d6c54d-7f15-4b18-b0df-8bed206f4609)

### Procedimiento :electric_plug:
Desconectamos la placa de la PC 

El esquema de conexión va a ser el siguiente:
* MOSI -> pin 11 en Arduino Uno
* MISO -> pin 12 en Arduino Uno
* SCK -> pin 13 en Arduino Uno
* CS -> pin 10  en Arduino Uno
* + (calle roja) -> VCC en Módulo micro SD
* - (calle azul) -> GND en Módulo micro SD

Volvemos a conectar la placa a la PC y revisamos en el IDE que el puerto y el tipo de placa sean los correctos.

Colocamos la memoria SD formateada en el módulo micro SD. Podemos formatear la memoria SD de la siguiente manera: la conectamos a una PC, hacemos botón derecho sobre la unidad, elegimos Formatear..., luego elegimos Sistema de archivos: FAT32 y le ponemos un nombre a la unidad. Advertencia: esto borra TODOS los archivos de la memoria, de forma irreversible.

Lo primero que vamos a hacer es ver si el módulo está funcionando. Para esto vamos a correr [este código](https://drive.google.com/drive/folders/1Auk2bpi2UsZg1wRcR5S4bSVcaOQSAcNh?usp=sharing) de prueba del módulo y ver si lee una tarjeta. Este código fue creado por Limor Fried y modificado por Tom Igoe. El código cargado necesita de las librerías SD y SPI, pero estas ya vienen instaladas por defecto con el Arduino IDE.

Si el módulo está funcionando, tenemos que poder ver en el monitor serial que la placa detecta al módulo micro SD y otras características de la tarjeta. 

Opcionalmente, podemos probar el siguiente [código](https://drive.google.com/drive/folders/1CoSecuAWTeewKR29v6g7QjIeroU6Y_bo?usp=sharing) para escribir en la memoria y después chequear en la pc que realmente se haya escrito en ella. Este último código es una modificación de ReadWrite de la biblioteca SD.

## Tercer paso, agregando información temporal :clock930:
Ahora necesitamos tener una marca temporal para cada una de las mediciones que hagamos. Si bien cuando medimos conectados a una computadora tenemos esa información, la idea es tener registradores de datos portátiles.

### Materiales
* Circuito con la protoboard, el Arduino Uno y el DHT11 que armamos en el primer paso
* [módulo RTC ds3231](https://articulo.mercadolibre.com.ar/MLA-916734561-modulo-rtc-ds3231-alta-precision-eeprom-24c32-arduino-cpila-_JM?quantity=4)
* [pila cr2032](https://www.mercadolibre.com.ar/pila-panasonic-cr2032-boton-pack-de-5-unidades/p/MLA16144305?pdp_filters=category:MLA6384#searchVariation=MLA16144305&position=1&search_layout=stack&type=product&tracking_id=7f628464-07a2-4d25-b8d9-152a70706ea2)

### Procedimiento :electric_plug:
Lo que tenemos que hacer ahora es poner en hora el RTC (Real Time Clock, que significa reloj en tiempo real). Este módulo es el que gracias a la pila cr2032 nos va a indicar la hora del registrador de datos aún cuando nuestra placa microcontroladora se quede sin energía. 

Conexionado:
![image](https://user-images.githubusercontent.com/58541097/188049010-8401e1f7-a56b-4799-9026-1bc9df3c8201.png)  
_Imagen tomada de Cave Pearl Proyect, sobre como armar un registrador de datos con un Arduino Uno para principiantes_

    Pin SCL: al pin A5 de Arduino UNO/nano
    Pin SDA: AL pin A4 de Arduino UNO/nano
    Pin SQW: al pin 2 de Arduino UNO/nano
    Pin Vcc: A 5V de Arduino
    Pin GND: A masa de Arduino
    
Para poner en hora el vamos a usar el siguiente [código](https://drive.google.com/drive/folders/1xmDleDZfOQ61mQbiiEyEOe9PkdY9zb3K?usp=sharing). En este código, en la linea 13 ponemos la hora como se encuentra ejemplificado en el comentario de la linea 14 para nuestro fecha y horario actual.

## Cuarto paso, uniendo todo...
Ahora lo que vamos a hacer es unir todos los pasos que acabamos de hacer para armar un registrador de datos. El sensor DHT 11 nos va a medir la humedad y la temperatura. Con el módulo micro SD registraremos la información en una memoria SD. Y con el módulo RTC tendremos la marca temporal de las mediciones independientemente de que la placa arduino se quede sin alimentación. 

### Materiales
* Todos los que ya tenemos

### Procedimiento :electric_plug:
Vamos a hacer las mismas conexiones que hicimos en los pasos anteriores. Esta vez también vamos a necesitar una librería, [Low Power](https://drive.google.com/file/d/1o-YGQhqMeBoLcBzYglsBMXcvMwZ6bN6o/view?usp=sharing), que nos va permitir poner a dormir al registrador de datos para reducir su consumo y así aumentar su autonomía. Ahora, vamos a usar el siguente [código](https://drive.google.com/drive/folders/1gBKL7R--h3a7O_cv_qDV6s0UkdjrKHTQ?usp=sharing), el cual es una modificación del que está [página de Cave Pearl Project](https://thecavepearlproject.org/2015/12/22/arduino-uno-based-data-logger-with-no-soldering/). Lo que lograremos con esto será registrar valores ambientales, que se registren en la memoria y que el registrador entre en fase de dormición hasta la próxima lectura.

# Miniaturizando...
Proximamente veremos como armar un registrador de datos con arduino Pro Mini de 3.3v. La primera dificultad es conectarlo a una PC... acá va un [tutorial del procedimiento](https://naylampmechatronics.com/blog/14_tutorial-como-programar-un-arduino-pro-mini-328.html)


